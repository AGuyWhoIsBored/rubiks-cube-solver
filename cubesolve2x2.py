# Rubik's Cube Solver - 2x2 Solve Module
# --------------------
# This module will contain all functions and data related to solving the 2x2 Rubik's Cube.
# Module created 12/19/2019
# -------------------- 

# Module version
version = "1.0"

def solve2x2Default(inputScramble): pass

# 2x2 cube data structure
class cube2x2():
    # constructor
    def __init__(self): pass