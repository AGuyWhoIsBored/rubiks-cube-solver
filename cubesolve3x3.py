# Rubik's Cube Solver - 3x3 Solve Module
# --------------------
# This module will contain all functions and data related to solving the 3x3 Rubik's Cube.
# Module created 12/19/2019
# -------------------- 

# Module version
version = "1.0"

# 3x3 cube data structure
class cube3x3():
    #boilerplate methods

    # constructor - will create a 3x3 cube in the solved position
    # G - green, R - red, B - blue, O - orange, Y - yellow, W - white
    # f - front, l - left, b - back, r - right, T - top, B - bottom
    def __init__(self, frontData = ['G']*9, leftData = ['O']*9, backData = ['B']*9, rightData = ['R']*9, topData = ['W']*9, bottomData = ['Y']*9): # array of 9 for each of the squares on face
        self.front = frontData
        self.left = leftData
        self.back = backData
        self.right = rightData
        self.top = topData
        self.bottom = bottomData

    # equality
    def __eq__(self, other): return type(other) == cube3x3 and self.front == other.front and self.left == other.left and self.back == other.back and self.right == other.right and self.top == other.top and self.bottom == other.bottom

    # repr
    def __repr__(self): return "3x3 Cube object with\nfront side  {}\nleft side   {}\nback side   {}\nright side  {}\ntop side    {}\nbottom side {}".format(self.front, self.left, self.back, self.right, self.top, self.bottom)

    # cube manipulation methods

    # rotation - returns a new cube3x3 instance
    def rotateLeft(self):  return cube3x3(self.right, self.front, self.left, self.back, self.top, self.bottom)
    def rotateRight(self): return cube3x3(self.left, self.back, self.right, self.front, self.top, self.bottom)
    def rotateUp(self):    return cube3x3(self.bottom, self.left, self.top, self.right, self.front, self.back)
    def rotateDown(self):  return cube3x3(self.top, self.left, self.bottom, self.right, self.back, self.front)

    # side manipulation - returns a new cube3x3 instance
    # all manipulatinos tested all four rotations (all passed) 
    def moveFrontC(self):         
        return \
        cube3x3([self.front[6], self.front[3], self.front[0], self.front[7], self.front[4], self.front[1], self.front[8], self.front[5], self.front[2]],
                [self.left[0], self.left[1], self.bottom[0], self.left[3], self.left[4], self.bottom[1], self.left[6], self.left[7], self.bottom[2]],   
                self.back,
                [self.top[6], self.right[1], self.right[2], self.top[7], self.right[4], self.right[5], self.top[8], self.right[7], self.right[8]],     
                [self.top[0], self.top[1], self.top[2], self.top[3], self.top[4], self.top[5], self.left[8], self.left[5], self.left[2]],              
                [self.right[6], self.right[3], self.right[0], self.bottom[3], self.bottom[4], self.bottom[5], self.bottom[6], self.bottom[7], self.bottom[8]])

    def moveFrontCC(self):  
        return \
        cube3x3([self.front[2], self.front[5], self.front[8], self.front[1], self.front[4], self.front[7], self.front[0], self.front[3], self.front[6]],
                [self.left[0], self.left[1], self.top[8], self.left[3], self.left[4], self.top[7], self.left[6], self.left[7], self.top[6]],
                self.back,
                [self.bottom[2], self.right[1], self.right[2], self.bottom[1], self.right[4], self.right[5], self.bottom[0], self.right[7], self.right[8]],
                [self.top[0], self.top[1], self.top[2], self.top[3], self.top[4], self.top[5], self.right[0], self.right[3], self.right[6]],
                [self.left[2], self.left[5], self.left[8], self.bottom[3], self.bottom[4], self.bottom[5], self.bottom[6], self.bottom[7], self.bottom[8]])
                                                     
    def moveLeftC(self):          
        return \
        cube3x3([self.top[0], self.front[1], self.front[2], self.top[3], self.front[4], self.front[5], self.top[6], self.front[7], self.front[8]], 
                [self.left[6], self.left[3], self.left[0], self.left[7], self.left[4], self.left[1], self.left[8], self.left[5], self.left[2]], 
                [self.back[0], self.back[1], self.bottom[6], self.back[3], self.back[4], self.bottom[3], self.back[6], self.back[7], self.bottom[0]], 
                self.right, 
                [self.back[8], self.top[1], self.top[2], self.back[5], self.top[4], self.top[5], self.back[2], self.top[7], self.top[8]], 
                [self.front[0], self.bottom[1], self.bottom[2], self.front[3], self.bottom[4], self.bottom[5], self.front[6], self.bottom[7], self.bottom[8]])

    def moveLeftCC(self):   
        return \
        cube3x3([self.bottom[0], self.front[1], self.front[2], self.bottom[3], self.front[4], self.front[5], self.bottom[6], self.front[7], self.front[8]], 
                [self.left[2], self.left[5], self.left[8], self.left[1], self.left[4], self.left[7], self.left[0], self.left[3], self.left[6]], 
                [self.back[0], self.back[1], self.top[6], self.back[3], self.back[4], self.top[3], self.back[6], self.back[7], self.top[0]], 
                self.right, 
                [self.front[0], self.top[1], self.top[2], self.front[3], self.top[4], self.top[5], self.front[6], self.top[7], self.top[8]], 
                [self.back[8], self.bottom[1], self.bottom[2], self.back[5], self.bottom[4], self.bottom[5], self.back[2], self.bottom[7], self.bottom[8]])

    def moveBackC(self):          
        return \
        cube3x3(self.front, 
                [self.top[2], self.left[1], self.left[2], self.top[1], self.left[4], self.left[5], self.top[0], self.left[7], self.left[8]], 
                [self.back[6], self.back[3], self.back[0], self.back[7], self.back[4], self.back[1], self.back[8], self.back[5], self.back[2]], 
                [self.right[0], self.right[1], self.bottom[8], self.right[3], self.right[4], self.bottom[7], self.right[6], self.right[7], self.bottom[6]], 
                [self.right[2], self.right[5], self.right[8], self.top[3], self.top[4], self.top[5], self.top[6], self.top[7], self.top[8]], 
                [self.bottom[0], self.bottom[1], self.bottom[2], self.bottom[3], self.bottom[4], self.bottom[5], self.left[0], self.left[3], self.left[6]])

    def moveBackCC(self):   
        return \
        cube3x3(self.front, 
                [self.bottom[6], self.left[1], self.left[2], self.bottom[7], self.left[4], self.left[5], self.bottom[8], self.left[7], self.left[8]], 
                [self.back[2], self.back[5], self.back[8], self.back[1], self.back[4], self.back[7], self.back[0], self.back[3], self.back[6]], 
                [self.right[0], self.right[1], self.top[0], self.right[3], self.right[4], self.top[1], self.right[6], self.right[7], self.top[2]], 
                [self.left[6], self.left[3], self.left[0], self.top[3], self.top[4], self.top[5], self.top[6], self.top[7], self.top[8]], 
                [self.bottom[0], self.bottom[1], self.bottom[2], self.bottom[3], self.bottom[4], self.bottom[5], self.right[8], self.right[5], self.right[2]])

    def moveRightC(self):         
        return \
        cube3x3([self.front[0], self.front[1], self.bottom[2], self.front[3], self.front[4], self.bottom[5], self.front[6], self.front[7], self.bottom[8]],  
                self.left, 
                [self.top[8], self.back[1], self.back[2], self.top[5], self.back[4], self.back[5], self.top[2], self.back[7], self.back[8]], 
                [self.right[6], self.right[3], self.right[0], self.right[7], self.right[4], self.right[1], self.right[8], self.right[5], self.right[2]], 
                [self.top[0], self.top[1], self.front[2], self.top[3], self.top[4], self.front[5], self.top[6], self.top[7], self.front[8]], 
                [self.bottom[0], self.bottom[1], self.back[6], self.bottom[3], self.bottom[4], self.back[3], self.bottom[6], self.bottom[7], self.back[0]])

    def moveRightCC(self):  
        return \
        cube3x3([self.front[0], self.front[1], self.top[2], self.front[3], self.front[4], self.top[5], self.front[6], self.front[7], self.top[8]], 
                self.left, 
                [self.bottom[8], self.back[1], self.back[2], self.bottom[5], self.back[4], self.back[5], self.bottom[2], self.back[7], self.back[8]], 
                [self.right[2], self.right[5], self.right[8], self.right[1], self.right[4], self.right[7], self.right[0], self.right[3], self.right[6]], 
                [self.top[0], self.top[1], self.back[6], self.top[3], self.top[4], self.back[3], self.top[6], self.top[7], self.back[0]], 
                [self.bottom[0], self.bottom[1], self.front[2], self.bottom[3], self.bottom[4], self.front[5], self.bottom[6], self.bottom[7], self.front[8]])
                                  
    def moveTopC(self):           
        return \
        cube3x3([self.right[0], self.right[1], self.right[2], self.front[3], self.front[4], self.front[5], self.front[6], self.front[7], self.front[8]], 
                [self.front[0], self.front[1], self.front[2], self.left[3], self.left[4], self.left[5], self.left[6], self.left[7], self.left[8]], 
                [self.left[0], self.left[1], self.left[2], self.back[3], self.back[4], self.back[5], self.back[6], self.back[7], self.back[8]], 
                [self.back[0], self.back[1], self.back[2], self.right[3], self.right[4], self.right[5], self.right[6], self.right[7], self.right[8]], 
                [self.top[6], self.top[3], self.top[0], self.top[7], self.top[4], self.top[1], self.top[8], self.top[5], self.top[2]], 
                self.bottom)

    def moveTopCC(self):    
        return \
        cube3x3([self.left[0], self.left[1], self.left[2], self.front[3], self.front[4], self.front[5], self.front[6], self.front[7], self.front[8]], 
                [self.back[0], self.back[1], self.back[2], self.left[3], self.left[4], self.left[5], self.left[6], self.left[7], self.left[8]], 
                [self.right[0], self.right[1], self.right[2], self.back[3], self.back[4], self.back[5], self.back[6], self.back[7], self.back[8]], 
                [self.front[0], self.front[1], self.front[2], self.right[3], self.right[4], self.right[5], self.right[6], self.right[7], self.right[8]], 
                [self.top[2], self.top[5], self.top[8], self.top[1], self.top[4], self.top[7], self.top[0], self.top[3], self.top[6]], 
                self.bottom)

    def moveBottomC(self):        
        return \
        cube3x3([self.front[0], self.front[1], self.front[2], self.front[3], self.front[4], self.front[5], self.left[6], self.left[7], self.left[8]], 
                [self.left[0], self.left[1], self.left[2], self.left[3], self.left[4], self.left[5], self.back[6], self.back[7], self.back[8]], 
                [self.back[0], self.back[1], self.back[2], self.back[3], self.back[4], self.back[5], self.right[6], self.right[7], self.right[8]], 
                [self.right[0], self.right[1], self.right[2], self.right[3], self.right[4], self.right[5], self.front[6], self.front[7], self.front[8]], 
                self.top,
                [self.bottom[6], self.bottom[3], self.bottom[0], self.bottom[7], self.bottom[4], self.bottom[1], self.bottom[8], self.bottom[5], self.bottom[2]])

    def moveBottomCC(self): 
        return \
        cube3x3([self.front[0], self.front[1], self.front[2], self.front[3], self.front[4], self.front[5], self.right[6], self.right[7], self.right[8]], 
                [self.left[0], self.left[1], self.left[2], self.left[3], self.left[4], self.left[5], self.front[6], self.front[7], self.front[8]], 
                [self.back[0], self.back[1], self.back[2], self.back[3], self.back[4], self.back[5], self.left[6], self.left[7], self.left[8]], 
                [self.right[0], self.right[1], self.right[2], self.right[3], self.right[4], self.right[5], self.back[6], self.back[7], self.back[8]], 
                self.top, 
                [self.bottom[2], self.bottom[5], self.bottom[8], self.bottom[1], self.bottom[4], self.bottom[7], self.bottom[0], self.bottom[3], self.bottom[6]]) 

    # checking for solves

    # checking for single side solve
    def isSideSolved(self, side):
        sideCheck = {
            'f': self.front.count(self.front[0]) == 9,
            'l': self.left.count(self.left[0]) == 9,
            'b': self.back.count(self.back[0]) == 9,
            'r': self.right.count(self.right[0]) == 9,
            'T': self.top.count(self.top[0]) == 9,
            'B': self.bottom.count(self.bottom[0]) == 9
        }
        return sideCheck.get(side)

    # checking for full solve
    def isSolved(self): return self.isSideSolved('f') and self.isSideSolved('l') and self.isSideSolved('r') and self.isSideSolved('b') and self.isSideSolved('T') and self.isSideSolved('B')

    # printing sides of the cube
    def printSide(self, side):
        sideRepr = { 'f': self.front, 'l': self.left, 'b': self.back, 'r': self.right, 'T': self.top, 'B': self.bottom }
        return sideRepr.get(side)

# All functions related to actually solving the cube!

# create new cube and manipulate with commands
# commands: F, f, Ba, ba, R, r, L, l, T, t, Bo, bo
# uppercase clockwise, lowercase counterclockwise
def generateFromScramble(inputScramble): 
    cube = cube3x3()
    for move in inputScramble:
        if   move == "F": cube.moveFrontC()
        elif move == "f": cube.moveFrontCC()
        elif move == "B": cube.moveBackC()
        elif move == "b": cube.moveBackCC()
        elif move == "R": cube.moveRightC()
        elif move == "r": cube.moveRightCC()
        elif move == "L": cube.moveLeftC()
        elif move == "l": cube.moveLeftCC()
        elif move == "T": cube.moveTopC()
        elif move == "t": cube.moveTopCC()
        elif move == "B": cube.moveBottomC()
        elif move == "b": cube.moveBottomCC()
    return cube

# pass in list of ALL colors for entire cube (9 of front, left, back, right, top, bottom)
def generateFromFullRawData(fullRawData): return cube3x3(fullRawData[0:8], fullRawData[9:17], fullRawData[18:26], fullRawData[27:35], fullRawData[36:44], fullRawData[45:53])

def solve3x3Default(scrambledCube): pass