# Rubik's Cube Solver
# --------------------
# This project aims to solve an input scrambled Rubik's Cube using (eventually) various different algorithms.
# It is also meant to be a good exercise for my newly learned skills with Python. 
#
# Project started 12/19/2019
# -------------------- 

# Program syntax:
# "python3 rubikscubesolver.py [rubiksCubeType] [algorithm] [inputScramble]"
# rubiksCubeType:   the type of cube you would like to have solved (defined by availability of cube solving modules)
#       currently implemented cube types: 2x2, 3x3, 4x4, 5x5
# algorithm:        solving algorithm to be used with the specific cube type (defined by cube solving modules)
#       currently implemented algorithms: default (F2L)
# inputScramble:    file that has provides moves to create the desired cube scramble

# Imports
from sys import argv
from cubesolve2x2 import solve2x2Default
from cubesolve3x3 import solve3x3Default
from cubesolve4x4 import solve4x4Default
from cubesolve5x5 import solve5x5Default

# Global program variables
versionMain = "1.0"

# We will have different "Rubik's Cube Solver Modules" to import and pass control to 
# so we can make development very modular
# Attempts to parse arguments and send to delegator
# (none) --> (none)
def main():
    
    # Welcome message
    print("\n----------------------------------------")
    print("Welcome to the Rubik's Cube Solver v" + versionMain + "!")
    print("Written and developed by AGuyWhoIsBored")
    print("----------------------------------------\n")

    # Parse arguments
    try: delegateSolve(argv[1], argv[2], open(argv[3]).readlines())
    except IndexError:
        print("Incorrect amount of arguments, please check argument syntax")
        exit()
    except FileNotFoundError:
        print("Input scramble file not found, please check input scramble file")
        exit()
    except Exception as e:
        print("An error occured, please check argument syntax: " + e)
        exit()

# Delegates the requested solve job to the correct cube solving module
# (str, str, list) --> (none)
def delegateSolve(rubiksCubeType, algorithm, inputScramble): 
    # optimize this in the future using dictionaries 
    # default (F2L) solving algorithm delegation
    if rubiksCubeType == "2x2" and algorithm == "default":   solve2x2Default(inputScramble)
    elif rubiksCubeType == "3x3" and algorithm == "default": solve3x3Default(inputScramble)
    elif rubiksCubeType == "4x4" and algorithm == "default": solve4x4Default(inputScramble)
    elif rubiksCubeType == "5x5" and algorithm == "default": solve5x5Default(inputScramble)

# Main entry point into the program
if __name__ == "__main__": main()